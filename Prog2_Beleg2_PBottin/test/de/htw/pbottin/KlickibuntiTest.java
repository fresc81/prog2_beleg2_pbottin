/**
 * 
 */
package de.htw.pbottin;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.htw.pbottin.klickibunti.Field;
import de.htw.pbottin.klickibunti.Game;


/**
 * @author Paul Bottin
 *
 */
public class KlickibuntiTest
{
  
  Game game;
  
  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception
  {
    game = new Game("MadFrEsC", 16, 16, 5, 8L);
  }
  
  /**
   * 
   */
  @Test
  public void testNextTurn()
  {
    //System.out.println(game.getField());
    //game.getField().removeRow(new Field.Index(15, 15));
    //System.out.println(game.getField());
    game.nextTurn(7, 7);
  }
  
}
