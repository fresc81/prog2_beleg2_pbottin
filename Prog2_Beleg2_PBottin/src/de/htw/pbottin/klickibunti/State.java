/**
 * 
 */
package de.htw.pbottin.klickibunti;


/**
 * The win/loose state of the game.
 * 
 * @author Paul Bottin
 *
 */
public enum State
{
  /**
   * the game is not yet over, the player may choose his next turn
   */
  PROGRESS,
  
  /**
   * the game is over and the player has won
   */
  WON,
  
  /**
   * the game is over and the player has lost
   */
  LOST;
  
}
