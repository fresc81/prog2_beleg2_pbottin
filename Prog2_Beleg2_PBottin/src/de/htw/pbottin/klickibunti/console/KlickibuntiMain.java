/**
 * 
 */
package de.htw.pbottin.klickibunti.console;

import java.io.PrintStream;
import java.util.Scanner;

import de.htw.pbottin.klickibunti.Game;
import de.htw.pbottin.klickibunti.State;
import de.htw.pbottin.klickibunti.Turn;


/**
 * This class is the main class for the console version of the Klickibunti game.
 * 
 * @author Paul Bottin
 *
 */
public class KlickibuntiMain
{
  
  /**
   * 
   */
  private KlickibuntiMain()
  {
  }
  
  /**
   * Starts the console version of the Klickibunti game.
   * 
   * @param args
   */
  public static void main(String[] args)
  {
    Game game = new Game("Player", 16, 16, 5);
    
    PrintStream out = System.out;
    Scanner in = new Scanner(System.in);
    State state = State.PROGRESS;
    Turn turn = null;
    while (state == State.PROGRESS)
    {
      // print field and score
      out.println(game.getField());
      out.printf("score: %d\n", game.getScore());
      
      // ask x
      out.print("x: ");
      while (!in.hasNextInt())
      {
        in.next();
        out.println("invalid value for x");
        out.print("x: ");
      }
      int x = in.nextInt();
      
      // ask y
      out.print("y: ");
      while (!in.hasNextInt())
      {
        in.next();
        out.println("invalid value for y");
        out.print("y: ");
      }
      int y = in.nextInt();
      
      // do turn
      try
      {
        turn  = game.nextTurn(x, y);
        state = turn.getState();
        out.printf("points: %d\n", turn.getScore());
      } catch (Exception e)
      {
        out.println(e.getMessage());
      }
      
    }
    
    // print field, score and result
    out.println(game.getField());
    out.printf("score: %d\n", game.getScore());
    out.printf("Game Over! %s\n", state==State.LOST?"You have lost.":"You have won.");
    
    in.close();
  }
  
}
