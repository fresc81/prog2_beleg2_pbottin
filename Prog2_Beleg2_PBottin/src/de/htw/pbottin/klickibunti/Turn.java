/**
 * 
 */
package de.htw.pbottin.klickibunti;


/**
 * This class stores all information that is needed to represent a single turn in the game.
 * It stores the current game field, the archived score for this particular turn and the win/loose state of the game.
 * 
 * @author Paul Bottin
 *
 */
public class Turn
{
  
  /**
   * the game field
   */
  private final Field field;
  
  /**
   * the score for this turn
   */
  private final int score;
  
  /**
   * if this game is over or still in progress
   */
  private final State state;
  
  /**
   * creates a new turn
   * 
   * @param field the game field
   * @param score the score for this turn
   * @param state if this game is over or still in progress
   */
  public Turn(Field field, int score, State state)
  {
    this.field = field;
    this.score = score;
    this.state = state;
  }
  
  /**
   * get the game field
   * @return the field
   */
  public Field getField()
  {
    return field;
  }
  
  /**
   * get the score for this turn
   * @return the score
   */
  public int getScore()
  {
    return score;
  }
  
  /**
   * get a value indicating if this game is over or still in progress
   * @return the state
   */
  public State getState()
  {
    return state;
  }
  
}
