package de.htw.pbottin.klickibunti.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import de.htw.pbottin.klickibunti.Highscore;

/**
 * This class implements the GUI dialog for showing the list of highscores.
 * 
 * @author Paul Bottin
 *
 */
public class HighscoresDialog extends JDialog
{
  
  /**
   * 
   */
  private static final long serialVersionUID = 2145244984439310550L;
  
  /**
   * the content panel
   */
  private JPanel contentPanel;

  /**
   * the layout
   */
  private GridBagLayout gbl_contentPanel;

  /**
   * Launch the application.
   * 
   * @param args the command line arguments
   */
  public static void main(String[] args)
  {
    try
    {
      HighscoresDialog dialog = new HighscoresDialog(null);
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      dialog.setVisible(true);
    } catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * Create the dialog.
   * 
   * @param owner the owning window
   */
  public HighscoresDialog(Frame owner)
  {
    super(owner);
    setTitle("Highscores");
    setBounds(100, 100, 300, 250);
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setModal(true);
    getContentPane().setLayout(new BorderLayout());
    Font f = null;
    {
      JPanel northPanel = new JPanel();
      getContentPane().add(northPanel, BorderLayout.CENTER);
      northPanel.setLayout(new BorderLayout(0, 0));
      {
        contentPanel = new JPanel();
        northPanel.add(contentPanel, BorderLayout.NORTH);
        gbl_contentPanel = new GridBagLayout();
        gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, 0.0};
        contentPanel.setLayout(gbl_contentPanel);
        {
          JLabel lblRank = new JLabel("Rank", JLabel.RIGHT);
          f = lblRank.getFont();
          GridBagConstraints gbc_lblRank = new GridBagConstraints();
          gbc_lblRank.anchor = GridBagConstraints.SOUTHEAST;
          gbc_lblRank.insets = new Insets(0, 0, 0, 5);
          gbc_lblRank.gridx = 0;
          gbc_lblRank.gridy = 0;
          gbc_lblRank.ipadx = 10;
          contentPanel.add(lblRank, gbc_lblRank);
        }
        {
          JLabel lblName = new JLabel("Name", JLabel.LEFT);
          GridBagConstraints gbc_lblName = new GridBagConstraints();
          gbc_lblName.anchor = GridBagConstraints.SOUTHWEST;
          gbc_lblName.insets = new Insets(0, 0, 0, 5);
          gbc_lblName.gridx = 1;
          gbc_lblName.gridy = 0;
          gbc_lblName.ipadx = 10;
          contentPanel.add(lblName, gbc_lblName);
        }
        {
          JLabel lblScore = new JLabel("Score", JLabel.LEFT);
          GridBagConstraints gbc_lblScore = new GridBagConstraints();
          gbc_lblScore.anchor = GridBagConstraints.SOUTHWEST;
          gbc_lblScore.insets = new Insets(0, 0, 0, 5);
          gbc_lblScore.gridx = 2;
          gbc_lblScore.gridy = 0;
          gbc_lblScore.ipadx = 10;
          contentPanel.add(lblScore, gbc_lblScore);
        }
      }
    }
    {
      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
      getContentPane().add(buttonPane, BorderLayout.SOUTH);
      {
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            setVisible(false);
            dispose();
          }
        });
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
      }
    }
    
    Highscore instance = Highscore.getInstance();
    int i = 1;
    GridBagConstraints c = new GridBagConstraints();
    f = f.deriveFont(Font.PLAIN);
    c.insets = new Insets(0, 0, 0, 5);
    c.ipadx = 10;
    for (Highscore.HighscoreElement highscore : instance)
    {
      c.gridy = i;
      
      c.gridx = 0;
      c.fill = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.SOUTHEAST;
      JLabel indexLabel = new JLabel(Integer.toString(i++), JLabel.RIGHT);
      indexLabel.setFont(f);
      gbl_contentPanel.setConstraints(indexLabel, c);
      contentPanel.add(indexLabel);
      
      c.gridx = 1;
      c.fill = GridBagConstraints.HORIZONTAL;
      c.anchor = GridBagConstraints.SOUTHWEST;
      JLabel nameLabel = new JLabel(highscore.getName(), JLabel.LEFT);
      nameLabel.setFont(f);
      gbl_contentPanel.setConstraints(nameLabel, c);
      contentPanel.add(nameLabel);
      
      c.gridx = 2;
      c.fill = GridBagConstraints.NONE;
      c.anchor = GridBagConstraints.SOUTHWEST;
      JLabel scoreLabel = new JLabel(Integer.toString(highscore.getScore()), JLabel.LEFT);
      scoreLabel.setFont(f);
      gbl_contentPanel.setConstraints(scoreLabel, c);
      contentPanel.add(scoreLabel);
    }
  }
  
  /**
   * show the dialog and block all input to the owning window
   */
  public void showDialog()
  {
    setVisible(true);
  }
  
}
