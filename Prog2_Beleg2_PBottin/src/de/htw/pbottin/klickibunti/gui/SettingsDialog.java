package de.htw.pbottin.klickibunti.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import de.htw.pbottin.klickibunti.Field;
import de.htw.pbottin.klickibunti.Game.Settings;


/**
 * This class implements the GUI for the game settings dialog.
 * 
 * @author Paul Bottin
 *
 */
public class SettingsDialog extends JDialog
{
  
  /**
   * 
   */
  private static final long serialVersionUID = 8727693986513070337L;
  private final JPanel contentPanel = new JPanel();
  private JLabel lblPlayer;
  private JLabel lblHeight;
  private JLabel lblWidth;
  private JLabel lblNumColors;
  private JTextField player;
  private JSpinner height;
  private JSpinner width;
  private JSpinner numColors;
  
  /**
   * did the user pressed okay?
   */
  private boolean okay;
  
  /**
   * Launch the application.
   * 
   * @param args the command line arguments
   */
  public static void main(String[] args)
  {
    try
    {
      SettingsDialog dialog = new SettingsDialog(null);
      dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
      dialog.setVisible(true);
    } catch (Exception e)
    {
      e.printStackTrace();
    }
  }
  
  /**
   * Create the dialog.
   * 
   * @param owner the owning window 
   */
  public SettingsDialog(Frame owner)
  {
    super(owner);
    okay = false;
    setTitle("Settings");
    setBounds(100, 100, 450, 300);
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setModal(true);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(new BorderLayout(0, 0));
    {
      JPanel panel = new JPanel();
      contentPanel.add(panel, BorderLayout.NORTH);
      panel.setLayout(new GridLayout(0, 2, 0, 0));
      {
        lblPlayer = new JLabel("Player: ");
        lblPlayer.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblPlayer);
      }
      {
        player = new JTextField();
        lblPlayer.setLabelFor(player);
        panel.add(player);
        player.setColumns(10);
      }
      {
        lblHeight = new JLabel("Height: ");
        lblHeight.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblHeight);
      }
      {
        height = new JSpinner();
        height.setModel(new SpinnerNumberModel(10, Field.MIN_HEIGHT, Field.MAX_HEIGHT, 1));
        lblHeight.setLabelFor(height);
        panel.add(height);
      }
      {
        lblWidth = new JLabel("Width: ");
        lblWidth.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblWidth);
      }
      {
        width = new JSpinner();
        width.setModel(new SpinnerNumberModel(16, Field.MIN_WIDTH, Field.MAX_WIDTH, 1));
        lblWidth.setLabelFor(width);
        panel.add(width);
      }
      {
        lblNumColors = new JLabel("Number of Colors: ");
        lblNumColors.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblNumColors);
      }
      {
        numColors = new JSpinner();
        numColors.setModel(new SpinnerNumberModel(4, Field.MIN_COLOR-1, Field.MAX_COLOR-1, 1));
        lblNumColors.setLabelFor(numColors);
        panel.add(numColors);
      }
    }
    {
      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
      getContentPane().add(buttonPane, BorderLayout.SOUTH);
      {
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            setVisible(false);
            dispose();
            okay = true;
          }
        });
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
      }
      {
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            setVisible(false);
            dispose();
          }
        });
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
      }
    }
  }
  
  /**
   * Initializes the dialog with the given settings and shows it.
   * All input to the owning window is blocked until the dialog has been closed.
   * @param settings the setting to initialize the dialog with
   * @return the new settings if the player clicked okay and the settings parameter if the user clicked cancel
   */
  public Settings showDialog(Settings settings)
  {
    player.setText(settings.getPlayerName());
    width.setValue(settings.getWidth());
    height.setValue(settings.getHeight());
    numColors.setValue(settings.getNumColors()-1);
    
    okay = false;
    setVisible(true);
    
    if (okay)
    {
      settings = new Settings(
        player.getText(),
        Integer.parseInt(width.getValue().toString()),
        Integer.parseInt(height.getValue().toString()),
        Integer.parseInt(numColors.getValue().toString())+1
      );
    }
    
    return settings;
  }
  
}
