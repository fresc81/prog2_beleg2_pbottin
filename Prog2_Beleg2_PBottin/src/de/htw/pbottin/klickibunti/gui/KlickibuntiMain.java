package de.htw.pbottin.klickibunti.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.border.EtchedBorder;

import de.htw.pbottin.klickibunti.Field.Index;
import de.htw.pbottin.klickibunti.Game;
import de.htw.pbottin.klickibunti.Game.Settings;
import de.htw.pbottin.klickibunti.Highscore;
import de.htw.pbottin.klickibunti.Turn;


/**
 * @author Paul Bottin
 *
 */
public class KlickibuntiMain
{
  
  /**
   * the colors to be used
   */
  private static final Color[] COLORS = {
    Color.WHITE,
    
    Color.RED,
    Color.BLUE,
    Color.YELLOW,
    
    Color.GREEN,
    Color.CYAN,
    Color.MAGENTA
  };
  
  /**
   * This class implements the clickable element in the game field.
   * It implements custom painting and a visual feedback if the mouse
   * cursor is moved over an element. 
   * 
   * @author Paul Bottin
   *
   */
  private class ColoredButton extends JButton
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = 2796829567692509644L;
    
    /**
     * mouse over this element or one of it's adjacencies
     */
    private boolean hovered = false;
    
    /**
     * the index of this element
     */
    private final Index index;
    
    /**
     * creates a new colored button element
     * 
     * @param index the index in the game field
     */
    public ColoredButton(Index index)
    {
      this.index = index;
    }
    
    /*
     * (non-Javadoc)
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    protected void paintComponent(Graphics g)
    {
      Color background = getBackground();
      boolean isWhite = Color.WHITE.equals(background);
      g.setColor(isWhite?background:(hovered?getBackground().brighter():getBackground().darker()));
      g.fillRect(0, 0, getWidth(), getHeight());
    }
    
    /*
     * (non-Javadoc)
     * @see javax.swing.AbstractButton#paintBorder(java.awt.Graphics)
     */
    @Override
    protected void paintBorder(Graphics g)
    {
      g.setColor(getForeground());
      g.drawRect(0, 0, getWidth()-1, getHeight()-1);
    }
    
    /*
     * (non-Javadoc)
     * @see javax.swing.JComponent#paintChildren(java.awt.Graphics)
     */
    @Override
    protected void paintChildren(Graphics g)
    {
    }
    
    /*
     * (non-Javadoc)
     * @see javax.swing.JComponent#processMouseEvent(java.awt.event.MouseEvent)
     */
    @Override
    protected void processMouseEvent(MouseEvent e)
    {
      super.processMouseEvent(e);
      switch(e.getID())
      {
      
      case MouseEvent.MOUSE_CLICKED:
        onMouseClicked();
        break;
      
      case MouseEvent.MOUSE_ENTERED:
        onMouseEntered();
        break;
        
      case MouseEvent.MOUSE_EXITED:
        onMouseExited();
        break;
        
      }
    }

    /**
     * to be done if the mouse cursor was moved away from this element
     */
    private void onMouseExited()
    {
      TreeSet<Index> adjacencies;
      adjacencies = new TreeSet<>();
      game.getField().findAdjacencies(index, adjacencies);
      
      for (Index index : adjacencies)
        buttons[index.getX()][index.getY()].hovered = false;
      
      panel.repaint();
    }

    /**
     * to be done if the mouse cursor was moved over this element
     */
    private void onMouseEntered()
    {
      TreeSet<Index> adjacencies;
      adjacencies = new TreeSet<>();
      game.getField().findAdjacencies(index, adjacencies);
      
      for (Index index : adjacencies)
        buttons[index.getX()][index.getY()].hovered = true;

      panel.repaint();
    }

    /**
     * to be done if this element was clicked
     */
    private void onMouseClicked()
    {
      // do in background so GUI doesn't get blocked
      executor.execute(new Runnable()
      {
        
        @Override
        public void run()
        {
          try
          {
            TreeSet<Index> adjacencies = new TreeSet<>();
            game.getField().findAdjacencies(getIndex(), adjacencies);
            
            Turn turn = game.nextTurn(getIndex());
            lblScore.setText(String.format("score: %d", game.getScore()));
            lblScore.validate();
            
            switch (turn.getState())
            {
            
            case PROGRESS:
              lblMessage.setText(String.format("points: %d", turn.getScore()));
              break;
              
            case LOST:
              lblMessage.setText(String.format("points: %d - You have lost!", turn.getScore()));
              executor.execute(new Runnable()
              {
                @Override
                public void run()
                {
                  playSound("loose.wav");
                  JOptionPane.showMessageDialog(frmKlickibunti, "You have lost!", "Game Over", JOptionPane.WARNING_MESSAGE);
                }
              });
              break;
              
            case WON:
              lblMessage.setText(String.format("points: %d - You have won!", turn.getScore()));
              executor.execute(new Runnable()
              {
                @Override
                public void run()
                {
                  // save highscore
                  Highscore.getInstance().addAndSaveHighscore(game.getPlayerName(), game.getScore());
                  
                  playSound("win.wav");
                  JOptionPane.showMessageDialog(frmKlickibunti, "You have won!", "Game Over", JOptionPane.WARNING_MESSAGE);
                  
                  // update highscores dialog
                  dlgHighscores.dispose();
                  dlgHighscores = new HighscoresDialog(frmKlickibunti);
                  dlgHighscores.showDialog();
                }
              });
              break;
              
            }
            lblMessage.validate();
            
            for (int i = 0; i < game.getWidth(); i++)
              for (int j = 0; j < game.getHeight(); j++)
                buttons[i][j].setBackground(COLORS[game.getField().getColor(i, j)]);
            
            for (Index index : adjacencies)
              buttons[index.getX()][index.getY()].hovered = false;
            
            adjacencies = new TreeSet<>();
            game.getField().findAdjacencies(index, adjacencies);
            
            for (Index index : adjacencies)
              buttons[index.getX()][index.getY()].hovered = true;
            
            panel.validate();
            panel.repaint();
            
          } catch (final Exception ex) {
            
            lblMessage.setText(String.format("error: %s!", ex.getMessage()));
            executor.execute(new Runnable()
            {
              @Override
              public void run()
              {
                playSound("error.wav");
                JOptionPane.showMessageDialog(frmKlickibunti, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
              }
            });
            
          }
        }
      });
    }
    
    /**
     * get the index of the corresponding element in the game field
     * 
     * @return the index of this element
     */
    public Index getIndex()
    {
      return index;
    }
    
  }
  
  private JFrame frmKlickibunti;
  
  /**
   * the game
   */
  private Game game;

  /**
   * an executor for background jobs
   */
  private ExecutorService executor;

  /**
   * the buttons that make up the game field
   */
  private ColoredButton[][] buttons;

  private JPanel panel;

  private JLabel lblScore;

  private JLabel lblMessage;
  
  /**
   * the settings dialog
   */
  private SettingsDialog dlgSettings;

  /**
   * the highscores dialog
   */
  private HighscoresDialog dlgHighscores;
  
  /**
   * Launch the application.
   * 
   * @param args the command line arguments
   *  
   */
  public static void main(String[] args)
  {
    EventQueue.invokeLater(new Runnable()
    {
      
      public void run()
      {
        try
        {
          KlickibuntiMain window = new KlickibuntiMain();
          window.frmKlickibunti.setVisible(true);
        } catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    });
  }
  
  /**
   * Create the application.
   */
  public KlickibuntiMain()
  {
    initialize();
  }
  
  /**
   * plays a soundfile
   * 
   * @param filename the filename of the wave file to be played 
   */
  private void playSound(String filename)
  {
    try
    {
      BufferedInputStream is = new BufferedInputStream(getClass().getResourceAsStream(filename));
      AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(is);
      Clip clip = AudioSystem.getClip();
      clip.open(audioInputStream);
      clip.start();
    } catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
  
  /**
   * creates a gamefield element button and associate it with the given coordinate
   * @param x the x position
   * @param y the y position
   * @return the game field element button
   */
  private ColoredButton createButton(int x, int y)
  {
    ColoredButton button = new ColoredButton(new Index(x, y));
    button.setBackground(COLORS[game.getField().getColor(x, y)]);
    buttons[x][y] = button;
    panel.add(button);
    return button;
  }
  
  /**
   * Initialize the contents of the frame.
   */
  private void initialize()
  {
    Settings settings = Settings.load();
    game = new Game(settings);
    executor = Executors.newSingleThreadExecutor();

    frmKlickibunti = new JFrame();
    frmKlickibunti.setIconImage(Toolkit.getDefaultToolkit().getImage(KlickibuntiMain.class.getResource("klickibunti.png")));
    frmKlickibunti.setTitle("Klickibunti");
    frmKlickibunti.setBounds(100, 100, 800, 600);
    frmKlickibunti.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frmKlickibunti.getContentPane().setLayout(new BorderLayout(0, 0));
    
    dlgSettings = new SettingsDialog(frmKlickibunti);
    dlgHighscores = new HighscoresDialog(frmKlickibunti);
    
    buttons = new ColoredButton[game.getWidth()][game.getHeight()];
    
    JToolBar toolBar = new JToolBar();
    frmKlickibunti.getContentPane().add(toolBar, BorderLayout.NORTH);
    
    JButton btnRestart = new JButton("Restart");
    btnRestart.setIcon(new ImageIcon(KlickibuntiMain.class.getResource("reload.png")));
    btnRestart.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        
        game.reset();
        for (int i = 0; i < game.getWidth(); i++)
          for (int j = 0; j < game.getHeight(); j++)
            buttons[i][j].setBackground(COLORS[game.getField().getColor(i, j)]);
        
        panel.validate();
        panel.repaint();
      }
    });
    toolBar.add(btnRestart);
    
    JButton btnUndo = new JButton("Undo");
    btnUndo.setIcon(new ImageIcon(KlickibuntiMain.class.getResource("back.png")));
    btnUndo.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        
        game.undoTurn();
        for (int i = 0; i < game.getWidth(); i++)
          for (int j = 0; j < game.getHeight(); j++)
            buttons[i][j].setBackground(COLORS[game.getField().getColor(i, j)]);
        
        panel.validate();
        panel.repaint();
      }
    });
    toolBar.add(btnUndo);
    
    JButton btnSettings = new JButton("Settings");
    btnSettings.setIcon(new ImageIcon(KlickibuntiMain.class.getResource("configure.png")));
    btnSettings.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        Settings settings = dlgSettings.showDialog(game.getSettings());
        if (settings != game.getSettings())
        {
          Settings.save(settings);
          game = new Game(settings);
          buttons = new ColoredButton[game.getWidth()][game.getHeight()];
          panel.removeAll();
          panel.setLayout(new GridLayout(game.getHeight(), game.getWidth(), 0, 0));
          for (int y = 0; y < game.getHeight(); y++)
            for (int x = 0; x < game.getWidth(); x++)
              createButton(x, y);
          
          panel.validate();
          panel.repaint();
        }
        
      }
    });
    toolBar.add(btnSettings);
    
    JButton btnExit = new JButton("Exit");
    JButton btnHighscores = new JButton("Highscores");
    btnHighscores.setIcon(new ImageIcon(KlickibuntiMain.class.getResource("/de/htw/pbottin/klickibunti/gui/highscores.png")));
    btnHighscores.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        
        dlgHighscores.showDialog();
        
      }
    });
    toolBar.add(btnHighscores);
    
    btnExit.setIcon(new ImageIcon(KlickibuntiMain.class.getResource("exit.png")));
    btnExit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        
        System.exit(0);
        
      }
    });
    toolBar.add(btnExit);
    
    final JSplitPane splitPane = new JSplitPane();
    splitPane.setResizeWeight(0.25);
    splitPane.setContinuousLayout(true);
    frmKlickibunti.getContentPane().add(splitPane, BorderLayout.SOUTH);
    
    lblScore = new JLabel("score: 0");
    splitPane.setLeftComponent(lblScore);
    
    lblMessage = new JLabel("");
    splitPane.setRightComponent(lblMessage);
    
    panel = new JPanel();
    panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
    frmKlickibunti.getContentPane().add(panel, BorderLayout.CENTER);
    panel.setLayout(new GridLayout(game.getHeight(), game.getWidth(), 0, 0));
    
    for (int y = 0; y < game.getHeight(); y++)
      for (int x = 0; x < game.getWidth(); x++)
        createButton(x, y);

    panel.validate();
    panel.repaint();
  }
  
}
