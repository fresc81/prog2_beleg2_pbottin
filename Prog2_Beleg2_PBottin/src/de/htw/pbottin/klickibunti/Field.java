/**
 * 
 */
package de.htw.pbottin.klickibunti;

import java.io.Serializable;
import java.util.Collection;
import java.util.Random;
import java.util.TreeSet;


/**
 * This class represents the gaming field for the Klickibunti game.
 * It consists of a 2-dimensional array of colors, which are mapped to
 * integers.
 * 
 * @author Paul Bottin
 *
 */
public class Field implements Serializable, Cloneable
{
  
  /**
   * the minimum width of a gaming field
   */
  public static final int MIN_WIDTH = 10;
  
  /**
   * the maximum width of a gaming field
   */
  public static final int MAX_WIDTH = 40;
  
  /**
   * the minimum height of a gaming field
   */
  public static final int MIN_HEIGHT = 10;
  
  /**
   * the maximum height of a gaming field
   */
  public static final int MAX_HEIGHT = 40;
  
  /**
   * the minimum number of colors
   */
  public static final int MIN_COLOR = 3;
  
  /**
   * the maximum number of colors
   */
  public static final int MAX_COLOR = 7;
  
  /**
   * A pointer to an element in the gaming field.
   * 
   * @author Paul Bottin
   *
   */
  public static class Index implements Serializable, Cloneable, Comparable<Index>
  {
    
    /*
     * (non-Javadoc)
     * @see java.io.Serializable
     */
    private static final long serialVersionUID = -6260775684787060840L;
    
    /**
     * the column index
     */
    private final int x;
    
    /**
     * the line index
     */
    private final int y;
    
    /**
     * Constructs an index object with the given column and line indices.
     * 
     * @param x the column index
     * @param y the line index
     */
    public Index(int x, int y)
    {
      this.x = x;
      this.y = y;
    }
    
    /**
     * Gets the column index.
     * @return the x
     */
    public int getX()
    {
      return x;
    }
    
    /**
     * Gets the line index.
     * @return the y
     */
    public int getY()
    {
      return y;
    }

    /**
     * Get the index above this index.
     * @return the next index above
     */
    public Index getUp()
    {
      return new Index(x, y-1);
    }

    /**
     * Get the index below this index.
     * @return the next index below
     */
    public Index getDown()
    {
      return new Index(x, y+1);
    }
    
    /**
     * Get the index right hand side of this index.
     * @return the next index on the right hand side
     */
    public Index getRight()
    {
      return new Index(x+1, y);
    }
    
    /**
     * Get the index left hand side of this index.
     * @return the next index on the left hand side
     */
    public Index getLeft()
    {
      return new Index(x-1, y);
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
      boolean result = false;
      if (obj instanceof Index)
      {
        Index lhs = (Index) obj;
        result = (x == lhs.x) && (y == lhs.y);
      }
      return result;
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
      return String.format("(%d; %d)", x, y);
    }
    
    /**
     * Gets the difference between two indices.
     * @param o the other index
     * @return the difference
     */
    @Override
    public int compareTo(Index o)
    {
      long d = (long)x*(long)MAX_HEIGHT + (long)y;
      long od = (long)o.x*(long)MAX_HEIGHT + (long)o.y;
      return (int)(d - od);
    }
    
  }
  
  /*
   * (non-Javadoc)
   * @see java.io.Serializable
   */
  private static final long serialVersionUID = 2886128225482754276L;
  
  /**
   * the width of this gaming field
   */
  private final int width;
  
  /**
   * the height of this gaming field
   */
  private final int height;

  /**
   * the number of colors in this gaming field
   */
  private final int numColors;
  
  /**
   * the elements of this gaming field
   */
  private final int[][] fieldColors;
  
  /**
   * Creates a copy of the given gaming field.
   * @param original the gaming field to copy from
   */
  public Field(Field original)
  {
    this(original.width, original.height, original.numColors);
    for (int x=0; x<width; x++)
      for (int y=0; y<height; y++)
        fieldColors[x][y] = original.fieldColors[x][y];
  }
  
  /**
   * Creates a new gaming field with the given parameters.
   * All elements are initialized with blank (white) color.
   * @param width  the number of columns
   * @param height the number of lines
   * @param numColors the number of colors this gaming field supports (including white)
   */
  public Field(int width, int height, int numColors)
  {
    if (!inRange(width, MIN_WIDTH, MAX_WIDTH))
      throw new IndexOutOfBoundsException("width must be in range ["+MIN_WIDTH+";"+MAX_WIDTH+"]");
    if (!inRange(height, MIN_HEIGHT, MAX_HEIGHT))
      throw new IndexOutOfBoundsException("height must be in range ["+MIN_HEIGHT+";"+MAX_HEIGHT+"]");
    if (!inRange(numColors, MIN_COLOR, MAX_COLOR))
      throw new IndexOutOfBoundsException("numColors must be in range ["+MIN_COLOR+";"+MAX_COLOR+"]");
    this.width = width;
    this.height = height;
    this.numColors = numColors;
    this.fieldColors = new int[width][height];
  }
  
  /**
   * Creates a new gaming field and initializes it with random colors generated by the supplied random number generator.
   * @param random the random number generator
   * @param width the number of columns
   * @param height the number of lines
   * @param numColors the number of colors this gaming field supports (including white)
   * @return a new gaming field with random colored elements
   */
  public static Field createRandom(Random random, int width, int height, int numColors)
  {
    Field result = new Field(width, height, numColors);
    for (int x=0; x<width; x++)
      for (int y=0; y<height; y++)
        result.fieldColors[x][y] = random.nextInt(numColors-1) + 1;
    return result;
  }
  
  /**
   * Range check
   * 0 <= x < m
   * @param x value to test
   * @param m maximum
   * @return true if in range
   */
  private static boolean inRange(int x, int m)
  {
    return (x >= 0) && (x < m);
  }
  
  /**
   * Range check
   * a <= x <= b
   * @param x value to test
   * @param a minimum
   * @param b maximum
   * @return true if in range
   */
  private static boolean inRange(int x, int a, int b)
  {
    return (x >= a) && (x <= b);
  }
  
  /**
   * Gets the number of colors this gaming field supports.
   * @return the numColors
   */
  public int getNumColors()
  {
    return numColors;
  }
  
  /**
   * Gets the number of columns in this gaming field.
   * @return the width
   */
  public int getWidth()
  {
    return width;
  }
  
  /**
   * Gets the number of lines in this gaming field.
   * @return the height
   */
  public int getHeight()
  {
    return height;
  }
  
  /**
   * Gets the color at the given index.
   * @param index a pointer to the element
   * @return the color at the given index
   */
  public int getColor(Index index)
  {
    if (index == null)
      throw new NullPointerException("index cannot be null");
    return getColor(index.x, index.y);
  }
  
  /**
   * Gets the color at the given position.
   * @param x the column
   * @param y the line
   * @return the color at the given position
   */
  public int getColor(int x, int y)
  {
    if (!inRange(x, width))
      return -1;
    if (!inRange(y, height))
      return -1;
    return fieldColors[x][y];
  }
  
  /**
   * Sets the color at the given position.
   * @param x the column
   * @param y the line
   * @param color the color to set at the given position
   */
  public void setColor(int x, int y, int color)
  {
    if (!inRange(x, width))
      throw new IndexOutOfBoundsException("x out of field bounds");
    if (!inRange(y, height))
      throw new IndexOutOfBoundsException("y out of field bounds");
    if (!inRange(color, numColors))
      throw new IndexOutOfBoundsException("color out of field bounds");
    fieldColors[x][y] = color;
  }
  
  /**
   * Converts the given color to a character.
   * White will be converted to a space character and all other colors
   * will be mapped to <code> 'A', 'B', 'C', ... </code>.
   * @param color the color to convert
   * @return the character that represents the given color
   */
  public static char colorToChar(int color)
  {
    char result;
    switch (color)
    {
    case -1:
      result = '#';
      break;
    case 0:
      result = ' ';
      break;
    default:
      result = (char)('A' + color - 1);
      break;
    }
    return result;
  }
  
  /**
   * Sets the color at the given index.
   * @param index a pointer to the element
   * @param color the color to set at the given index
   */
  public void setColor(Index index, int color)
  {
    if (index == null)
      throw new NullPointerException("index cannot be null");
    setColor(index.x, index.y, color);
  }
  
  /**
   * Finds all adjacent elements with the same color and stores their indices into the given collection.
   * @param index the starting position
   * @param adjacencies a collection that stores the found element indices
   */
  public void findAdjacencies(Index index, Collection<Index> adjacencies)
  {
    // only find adjacencies of fields not already in the set
    if (adjacencies.add(index))
    {
      int centerColor = getColor(index);
      
      // get surrounding colors...
      
      Index upIndex = index.getUp();
      int upColor = getColor(upIndex);
      
      Index downIndex = index.getDown();
      int downColor = getColor(downIndex);
      
      Index rightIndex = index.getRight();
      int rightColor = getColor(rightIndex);
      
      Index leftIndex = index.getLeft();
      int leftColor = getColor(leftIndex);
      
      // recurse all adjacencies with same color...
      
      if (centerColor == upColor)
        findAdjacencies(upIndex, adjacencies);
      
      if (centerColor == downColor)
        findAdjacencies(downIndex, adjacencies);
      
      if (centerColor == rightColor)
        findAdjacencies(rightIndex, adjacencies);
      
      if (centerColor == leftColor)
        findAdjacencies(leftIndex, adjacencies);
      
    }
  }
  
  /**
   * Finds the last empty column beginning at the given column index.
   * @param x the starting column index
   * @return the found column index or <code>-1</code> if there are no more empty columns
   */
  private int findLastEmptyColumnFront(int x)
  {
    if (x >= width)
      return -1;
    if (isEmptyColumn(x))
      return findLastEmptyColumnFront(x+1);
    return x;
  }
  
  /**
   * Tests if the given column is empty.
   * @param x the column index
   * @return a value indicating if the column is empty
   */
  private boolean isEmptyColumn(int x)
  {
    boolean result = true;
    for (int y = 0; y < height; y++)
      if (getColor(x, y) != 0)
        result = false;
    return result;
  }
  
  /**
   * Finds the first empty column beginning at the given column index and counting down towards 0.
   * @param x the starting column index
   * @return the found column index or <code>-1</code> if there are no more empty columns
   */
  private int findFirstEmptyColumnBack(int x)
  {
    if (x == -1)
      return -1;
    if (isEmptyColumn(x))
      return x;
    return findFirstEmptyColumnBack(x-1) ;
  }
  
  /**
   * Joins all empty columns and returns the number of columns eliminated.
   * @return the number of columns eliminated
   */
  public int joinEmptyColumns()
  {
    int joined = 0;
    
    int start = findLastEmptyColumnFront(0);
    int end = findFirstEmptyColumnBack(width-1);
    
    while (start < end)
    {
      removeColumn(end);
      ++joined;
      ++start;
      end = findFirstEmptyColumnBack(end);
    }
    
    return joined;
  }
  
  /**
   * Removes the given column and moves all previous columns to the right hand side.
   * @param column the column index
   */
  private void removeColumn(int column)
  {
    for (int y = 0; y < height; y++)
    {
      Index index = new Index(column, y);
      Index nextIndex = index.getLeft();
      int nextColor = getColor(nextIndex);
      
      while (nextColor >= 0)
      {
        setColor(index, nextColor > 0 ? nextColor : 0);
        
        index = nextIndex;
        nextIndex = index.getLeft();
        nextColor = getColor(nextIndex);
      }
      setColor(index, 0);
    }
  }
  
  /*
   * (non-Javadoc)
   * @see java.lang.Object#clone()
   */
  @Override
  public Object clone()
  {
    return new Field(this);
  }
  
  /*
   * (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder();
    int color;
    
    sb.append("   ");
    for (int x=0; x<width; x++)
      sb.append(String.format("%2d ", x));
    sb.append('\n');
    
    for (int y=0; y<height; y++)
    {
      sb.append(String.format("%2d ", y));
      for (int x=0; x<width; x++)
      {
        color = fieldColors[x][y];
        sb.append(' ');
        sb.append(colorToChar(color));
        sb.append(' ');
      }
      sb.append('\n');
    }
    return sb.toString();
  }
  
  /**
   * Computes the Win/Loose state of the game.
   * @return the state
   */
  public State computeState()
  {
    State state = State.PROGRESS;
    
    boolean hasWon = true;
    boolean hasLost = true;
    
    // if all colors == 0 -> won
    for (int x=0; x<width; x++)
      for (int y=0; y<height; y++)
        if (fieldColors[x][y] > 0)
        {
          hasWon = false;
          
          // if all remaining have no adjacencies -> lost
          TreeSet<Index> adjacencies = new TreeSet<>();
          findAdjacencies(new Index(x, y), adjacencies);
          if (adjacencies.size() > 1)
            hasLost = false;
          
        }
    
    if (hasWon)
      state = State.WON;
    else if (hasLost)
      state = State.LOST;
    
    return state;
  }
  
  /**
   * Eliminates blank fields and moves all elements above it down.
   */
  public void applyGravity()
  {
    boolean changed = true;
    
    while (changed)
    {
      changed = false;
      Index index, next;
      int color;
      for (int x=0; x<width; x++)
        for (int y=0; y<height; y++)
        {
          index = new Index(x, y);
          next = index.getDown();
          if ((getColor(index) != 0) && (getColor(next) == 0))
          {
            color = getColor(index);
            setColor(next, color);
            setColor(index, 0);
            changed = true;
          }
        }
    }
    
  }
  
}
