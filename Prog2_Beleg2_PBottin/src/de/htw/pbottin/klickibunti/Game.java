/**
 * 
 */
package de.htw.pbottin.klickibunti;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Random;
import java.util.TreeSet;

import de.htw.pbottin.klickibunti.Field.Index;


/**
 * This class represents the gamelogic for the Klickibunti-game.
 * It defines the methods <code>reset</code>, <code>nextTurn</code> and
 * <code>undoTurn</code> which alter the state of the game.<br>
 * It stores a collection of turns to provide undo-support up to the first
 * turn.
 * 
 * @author Paul Bottin
 *
 */
public class Game
{
  
  /**
   * This class represents the settings needed to start a new game.
   * These settings can be stored into and loaded from a file.
   * 
   * @author Paul Bottin
   *
   */
  public static class Settings implements Serializable
  {
    
    /**
     * serial number for the {@link Serializable} interface
     */
    private static final long serialVersionUID = 4443687004321697896L;
    
    /**
     * the default settings to be used if there are no stored settings
     */
    private static final Settings DEFAULT_SETTINGS = new Settings("Player", 16, 10, 5);
    
    /**
     * the player's name
     */
    private final String playerName;
    
    /**
     * the width of the field
     */
    private final int width;
    
    /**
     * the height of the field
     */
    private final int height;
    
    /**
     * the number of colors to be used (including the white color)
     */
    private final int numColors;
    
    /**
     * creates a <code>Settings</code>-object with the given values
     * @param playerName
     *  the player's name
     * @param width
     *  the width of the field
     * @param height
     *  the height of the field
     * @param numColors
     *  the number of colors to be used (including the white color)
     */
    public Settings(String playerName, int width, int height, int numColors)
    {
      this.playerName = playerName;
      this.width = width;
      this.height = height;
      this.numColors = numColors;
    }
    
    /**
     * retrieve the player's name
     * @return
     *  the player's name
     */
    public String getPlayerName()
    {
      return playerName;
    }
    
    /**
     * retrieve the width of the field
     * @return
     *  the width of the field
     */
    public int getWidth()
    {
      return width;
    }
    
    /**
     * retrieve the height of the field
     * @return
     *  the height of the field
     */
    public int getHeight()
    {
      return height;
    }
    
    /**
     * retrieve the number of colors to be used (including the white color)
     * @return
     *  the number of colors
     */
    public int getNumColors()
    {
      return numColors;
    }
    
    /**
     * stores the given settings into a file named <code>"settings.properties"</code>
     * @param settings
     *  the <code>Settings</code>-object to be stored
     */
    public static void save(Settings settings)
    {
      try
      {
        Properties properties = new Properties();
        properties.setProperty("playerName",  settings.playerName);
        properties.setProperty("width",       Integer.toString(settings.width));
        properties.setProperty("height",      Integer.toString(settings.height));
        properties.setProperty("numColors",   Integer.toString(settings.numColors));
        properties.store(new FileOutputStream("settings.properties"), "KlickiBunti Settings");
      } catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
    
    /**
     * tries to read settings from a file named <code>"settings.properties"</code>
     * @return
     *  a new <code>Settings</code>-object with the loaded settings or the <code>DEFAULT_SETTINGS</code> if no
     *  settings could be loaded
     */
    public static Settings load()
    {
      try
      {
        Properties properties = new Properties();
        properties.load(new FileInputStream("settings.properties"));
        String playerName = properties.getProperty("playerName",  DEFAULT_SETTINGS.playerName);
        int width = Integer.parseInt(properties.getProperty("width", Integer.toString(DEFAULT_SETTINGS.width)));
        int height = Integer.parseInt(properties.getProperty("height", Integer.toString(DEFAULT_SETTINGS.height)));
        int numColors = Integer.parseInt(properties.getProperty("numColors", Integer.toString(DEFAULT_SETTINGS.numColors)));
        return new Settings(playerName, width, height, numColors);
      } catch (Exception ex)
      {
        ex.printStackTrace();
      }
      return DEFAULT_SETTINGS;
    }
    
  }
  
  /**
   * The settings of this game instance. These settings are constant.
   */
  private final Settings settings;
  
  /**
   * A random number generator, it's start value (seed) can be specified in the constructor.
   * It generates the random color values at the start of the game.  
   */
  private final Random random;
  
  /**
   * The history of played turns. It is used as a Stack and contains at least the first turn.
   */
  private final LinkedList<Turn> history;

  /**
   * creates a new game with the given settings
   * @param playerName
   *    the player's name
   * @param width
   *    the width of the field
   * @param height
   *    the height of the field
   * @param numColors
   *    the number of colors
   */
  public Game(String playerName, int width, int height, int numColors)
  {
    this.settings = new Settings(playerName, width, height, numColors);
    this.random = new Random();
    this.history = new LinkedList<>();
    reset();
  }
  
  /**
   * creates a new game with the given settings as <code>Settings</code>-object
   * @param settings
   *    the game settings
   */
  public Game(Settings settings)
  {
    this.settings = settings;
    this.random = new Random();
    this.history = new LinkedList<>();
    reset();
  }
  
  /**
   * creates a new game with the given settings and a seed value for the random number generator
   * @param playerName
   *    the player's name
   * @param width
   *    the width of the field
   * @param height
   *    the height of the field
   * @param numColors
   *    the number of colors
   * @param seed
   *    the start value for the random number generator
   */
  public Game(String playerName, int width, int height, int numColors, long seed)
  {
    this.settings = new Settings(playerName, width, height, numColors);
    this.random = new Random(seed);
    this.history = new LinkedList<>();
    reset();
  }
  
  /**
   * creates a new game with the given settings as <code>Settings</code>-object and
   * a seed value for the random number generator
   * @param settings
   *    the game settings
   * @param seed
   *    the start value for the random number generator
   */
  public Game(Settings settings, long seed)
  {
    this.settings = settings;
    this.random = new Random(seed);
    this.history = new LinkedList<>();
    reset();
  }
  
  /**
   * retrieve the game settings
   * @return
   *    the settings
   */
  public Settings getSettings()
  {
    return settings;
  }
  
  /**
   * retrieve the width of the field
   * @return
   *    the width
   */
  public int getWidth()
  {
    return settings.width;
  }
  
  /**
   * retrieve the height of the field
   * @return
   *    the height
   */
  public int getHeight()
  {
    return settings.height;
  }
  
  /**
   * retrieve the number of colors in the field
   * @return
   *    the number of colors
   */
  public int getNumColors()
  {
    return settings.numColors;
  }
  
  /**
   * retrieve the player's name
   * @return
   *    the player's name
   */
  public String getPlayerName()
  {
    return settings.playerName;
  }
  
  /**
   * retrieve the current game field
   * @return
   *    the field
   */
  public synchronized Field getField()
  {
    return history.getFirst().getField();
  }
  
  /**
   * calculate and retrieve the score
   * @return
   *    the score
   */
  public synchronized int getScore()
  {
    int result = 0;
    for (Turn turn : history)
      result += turn.getScore();
    return result;
  }
  
  /**
   * retrieve the number of turns played
   * @return
   *    the number of turns played
   */
  public synchronized int getNumTurns()
  {
    return history.size()-1;
  }
  
  /**
   * reset the game, a new game with the same settings will be available
   */
  public synchronized void reset()
  {
    // clear history
    this.history.clear();
    
    // add a randomized field for the first turn
    this.history.addFirst(new Turn(Field.createRandom(random, settings.width, settings.height, settings.numColors), 0, State.PROGRESS));
  }
  
  /**
   * do a single turn and retrieve it
   * @param x
   *    the X-coordinate
   * @param y
   *    the Y-coordinate
   * @return
   *    a <code>Turn</code>-object that contains the new game state
   * @throws IllegalStateException
   *    if the index provided had an invalid color
   * @throws IllegalArgumentException
   *    if the index provided had no adjacencies with the same color
   */
  public synchronized Turn nextTurn(int x, int y)
  {
    // clone current field's state
    Turn turn = null;
    Field current = new Field(getField());
    int score = 0;
    
    // check color of selected field
    int color = current.getColor(x, y);
    if (color > 0)
    {
      // find all adjacencies with same color...
      TreeSet<Field.Index> adjacencies = new TreeSet<>();
      Field.Index index = new Field.Index(x, y);
      current.findAdjacencies(index, adjacencies);
      
      // if there are adjacencies
      if (adjacencies.size() > 1)
      {
        int joined;
        
        // calculate per color score 2*n-2
        score += 2 * adjacencies.size() - 2;
        
        // set color to white
        for (Index adjacency : adjacencies)
          current.setColor(adjacency, 0);
        
        // let colors fall down
        current.applyGravity();
        
        // join empty columns and calculate per column score 10*n
        joined = current.joinEmptyColumns();
        score += joined * 10;
        
        // take number of colors into account
        score *= getNumColors() - 2;
        
        // compute loose/win/progress state
        State state = current.computeState();
        
        // if the turn is okay store it in the history
        history.addFirst(turn = new Turn(current, score, state));
        
      } else // no adjacencies
        throw new IllegalArgumentException("no adjacencies");
      
    } else // illegal color
      throw new IllegalStateException("illegal color");
    
    // return value indicating the validity of the turn
    return turn;
  }
  
  /**
   * removes the previous turn from the history 
   * @throws IllegalStateException
   *    if trying to undo the first turn
   */
  public synchronized void undoTurn()
  {
    if (getNumTurns() == 0)
      throw new IllegalStateException("cannot undo first turn");
    
    // remove last turn from history
    history.poll();
  }

  /**
   * do a single turn and retrieve it
   * @param index the field index
   * @return
   *    a <code>Turn</code>-object that contains the new game state
   * @throws IllegalStateException
   *    if the index provided had an invalid color
   * @throws IllegalArgumentException
   *    if the index provided had no adjacencies with the same color
   */
  public Turn nextTurn(Index index)
  {
    return nextTurn(index.getX(), index.getY());
  }
  
}
