/**
 * 
 */
package de.htw.pbottin.klickibunti;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;


/**
 * This class implements the storage and retrieving of highscores. It uses the serialization facility
 * of the Java Virtual Machine and implements the List interface.
 * 
 * @author Paul Bottin
 *
 */
public class Highscore extends LinkedList<Highscore.HighscoreElement>
{
  
  /**
   * Represents an comparable element in the highscore list.
   * 
   * @author Paul Bottin
   *
   */
  public class HighscoreElement implements Serializable, Comparable<HighscoreElement>
  {
    
    /**
     * 
     */
    private static final long serialVersionUID = 8847249341749757025L;
    
    /**
     * the name of the player 
     */
    private final String name;
    
    /**
     * the score of the player
     */
    private final int score;
    
    /**
     * creates an highscore element
     *  
     * @param name the name of the player
     * @param score the score of the player
     */
    private HighscoreElement(String name, int score)
    {
      this.name = name;
      this.score = score;
    }
    
    /**
     * get the name of the player
     * 
     * @return the name of the player
     */
    public String getName()
    {
      return name;
    }
    
    /**
     * get the score of the player
     * 
     * @return the score of the player
     */
    public int getScore()
    {
      return score;
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(HighscoreElement o)
    {
      return Integer.compare(o.score, this.score);
    }
    
    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
      return String.format("%s %d", getName(), getScore());
    }
    
  }
  
  /**
   * 
   */
  private static final long serialVersionUID = -3538975765394194029L;
  
  /**
   * the maximum number of stored highscores
   */
  public static final int MAX_HIGHSCORES = 10;
  
  /**
   * the file to store the highscores to
   */
  private static final String HIGHSCORES_FILE = "highscores.bin";
  
  /**
   * the singleton instance, lazy instanciated by the getInstance() method
   */
  private static Highscore singletonInstance = null;
  
  /**
   * 
   */
  private Highscore()
  {
  }
  
  /**
   * gets the stored singleton instance of this class, highscores are automatically
   * loaded
   * 
   * @return the singleton Highscore instance
   */
  public static Highscore getInstance()
  {
    if (singletonInstance == null)
    {
      singletonInstance = loadInstance();
    }
    return singletonInstance;
  }

  /**
   * reads an Highscore instance from the highscores file
   * 
   * @return the new Highscore instance
   */
  private static Highscore loadInstance()
  {
    Highscore highscore = null;
    ObjectInputStream oin = null;
    try
    {
      oin = new ObjectInputStream(new BufferedInputStream(new FileInputStream(HIGHSCORES_FILE)));
      Object obj = oin.readObject();
      
      if (obj instanceof Highscore)
      {
        
        highscore = Highscore.class.cast(obj);
        
      } else
        throw new ClassCastException("not a highscore list in the highscores file");
      
    } catch (ClassCastException | ClassNotFoundException | IOException e)
    {
      
      e.printStackTrace();
      highscore = new Highscore();
      
    } finally {
      
      try
      {
        if (oin != null)
            oin.close();
      } catch (IOException e)
      {
        e.printStackTrace();
      }
      
    }
    return highscore;
  }
  
  /**
   * writes this Highscore instance to the highscores file
   */
  private static void saveInstance()
  {
    Highscore highscore = getInstance();
    ObjectOutputStream oout = null;
    
    try
    {
      oout = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(HIGHSCORES_FILE)));
      oout.writeObject(highscore);
      oout.flush();
      
    } catch (IOException e)
    {
      e.printStackTrace();
    } finally
    {
      
      try
      {
        if (oout != null)
          oout.close();
      } catch (IOException e)
      {
        e.printStackTrace();
      }
      
    }
    
  }
  
  /**
   * add a new highscore to the list, shrink the list to the maximum number
   * of stored highscore elements and stores the highscore list.
   *  
   * @param name the player name
   * @param score the archieved score of the player
   */
  public void addAndSaveHighscore(String name, int score)
  {
    // add to list
    HighscoreElement element = new HighscoreElement(name, score);
    addLast(element);
    
    // sort list
    Collections.sort(this);
    
    // shrink list
    while (size() > MAX_HIGHSCORES)
      pollLast();
    
    // save list
    saveInstance();
  }
  
}
